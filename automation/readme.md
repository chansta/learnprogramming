# Five Elements in Learning a Programming Language

## Introduction 

This is a collection of notebooks demonstrating the five basic elements underlying most programming languages. These elements are demonstrated through [Python](http://www.python.org) at the moment, but the plan is to include [R](http://www.r-project.org) and [Julia](http://www.julialang.org) in the future. The basic arugments are that 

    1. You do not need to know everyting about a language for it to be useful. 
    2. You learn what you need and it can be FUN. 

The five basic elements that will make a programming language useful for you are 

    1. Understanding data types and their operations. 
    2. Conditional Statements.
    3. Control or loops.
    4. Input and Output. 
    5. Custom defined function. 

Once learners understand these basic concepts and able to execute them through a language then the learners can readily starting using the language productively. They can alwya improve their skills by learning more about the languge, but they do not need to know everything before they can start. 

The notebooks contained in this repo aims to demonstrate these concepts in a particular language, starting with Python. 

## Code
    The folder Code contains the [Jupyter](https://jupyter.org) notebooks and their HTML version. 

## Data
    The folder Data contains all the data used in the notebooks and some of the outputs from the notebooks. 

** Presentation
    The folder Presentation contains a brief beamer presentation on automating tasks with programming languages. I used this in conjunction with the notebooks. 

